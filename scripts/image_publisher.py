#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""ROS module to publish single wrench image"""

import cv2
import rospy
import rospkg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

cv_bridge = CvBridge()

frame = cv2.imread(rospkg.RosPack().get_path('mascor_wrench_detection') + '/images/frame.jpg')
ros_img = Image()
ros_img = cv_bridge.cv2_to_imgmsg(frame, encoding="bgr8")

pub = rospy.Publisher("/camera/image_raw", Image, queue_size=1)


# initialize ROS node
rospy.init_node("image_publisher")

def main():
    """main function which starts the node"""
    rate = rospy.Rate(10)
    rospy.loginfo("Starting image publisher!")

    while not rospy.is_shutdown():
        pub.publish(ros_img)
        rate.sleep()

    rospy.loginfo("Bye!")

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
