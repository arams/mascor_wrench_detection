#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""ROS module to detect correct wrench for MBZIRC challenge 2"""

import numpy as np
import cv2
import rospy

from cv_bridge import CvBridge


# start your code here!




#cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
#cv2.imshow('frame',frame)
#cv2.waitKey(0)
#cv2.destroyAllWindows()


# initialize ROS node
rospy.init_node("wrench_detection")

# insert subscriber here

def main():
    """main function which starts the node"""
    rate = rospy.Rate(10)
    rospy.loginfo("Starting wrench detection!")

    while not rospy.is_shutdown():
        rate.sleep()

    rospy.loginfo("Bye!")

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass